**Крестики-нолики**

Поле представляет собой двумерный массив. Необходимо реализовать три функции: 

* *makeMoveX* - делает ход крестиком
* *makeMoveY* - делает ход ноликом
* *isFinished* - проверяет, завершена ли партия

Написать тесты на эти функции, обработать частные случаи (ход в занятую клетку, ход за пределы поля и т. д.)