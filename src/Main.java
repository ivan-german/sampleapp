import static org.junit.Assert.*;
import org.junit.*;

public class Main {

    public static void main(String[] args) {
        char[][] field = new char[3][3];

        makeMoveX(field, 0, 0);
        makeMoveO(field, 1, 1);
        makeMoveX(field, 0, 1);
        makeMoveO(field, 1, 2);
        makeMoveX(field, 0, 2);

        System.out.println("Game finished: " + isFinished(field));
    }

    public static boolean makeMoveX(char[][] field, int row, int col) {
        return false;
    }

    public static boolean makeMoveO(char[][] field, int row, int col) {
        return false;
    }

    public static boolean isFinished(char[][] field) {
        return false;
    }

}
