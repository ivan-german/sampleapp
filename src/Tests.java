import org.junit.*;
import static org.junit.Assert.*;

public class Tests {

    @Test
    public void testTrue() {
        assertTrue(true);
    }

    @Test
    public void testFalse() {
        assertTrue(false);
    }

    @Test
    public void testMakeMoveX() {
        char[][] field = new char[3][3];

        boolean result = Main.makeMoveX(field, 0, 0);

        assertTrue(result);
    }

    @Test
    public void testMakeMoveXIsFieldChanged() {
        char[][] field = new char[3][3];

        Main.makeMoveX(field, 0, 0);

        assertEquals('X', field[0][0]);
    }

    @Test
    public void testMakeMoveXMoveOutsideField() {
        char[][] field = new char[3][3];

        boolean result = Main.makeMoveX(field, 3, 0);

        assertFalse(false);
    }

    @Test
    public void testMakeMoveXToNotEmptyCell() {
        char [][]field=new char[3][3];
        field[0][0]='X';
        boolean result= Main.makeMoveX(field, 0, 0);
        assertFalse(result);

    }

    @Test
    public void testMakeMoveXIsFieldNotChanged(){
        char[][]field = new char[3][3];
        field[0][0] = 'O';
        boolean result = Main.makeMoveX(field, 0, 0);
        assertEquals('O', field[0][0]);
    }
    @Test
    public void testMakeMoveO() {
        char[][] field = new char[3][3];

        boolean result = Main.makeMoveO(field, 0, 0);

        assertTrue(result);
    }
    @Test
    public void testMakeMoveOIsFieldChanged(){
        char[][] field= new char[3][3];
        boolean result=Main.makeMoveO(field, 0, 0);
        assertEquals('O',field[0][0]);

    }
}
